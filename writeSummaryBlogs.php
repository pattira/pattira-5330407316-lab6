<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <body>
<?php
 //header('Content-Type: text/html; charset=utf-8');
 
 $dom = new domDocument;
 $dom->load('http://www.gotoknow.org/blogs/posts?format=rss');
  
 $docXml = new domDocument('1.0','UTF-8');
 $elementItems = $docXml->appendChild($docXml->createElement('items'));
 
 $channel = $dom->documentElement;
 $elementItem = $channel->getElementsByTagName("item");
 
	echo "Reading from summaryBlogs.xml...<br/>";
		
	foreach ($elementItem as $item) {
		$elementTitle = $item->getElementsByTagName("title")->item(0)->nodeValue;
        $elementLink = $item->getElementsByTagName("link")->item(0)->nodeValue;
        $elementAuthor = $item->getElementsByTagName("author")->item(0)->nodeValue;
		
        echo $elementLink.$elementAuthor;
		
		$item = $docXml->createElement("item");
		$elementItems->appendChild($item);
		
		$title = $docXml->createElement("title");
		$item->appendChild($title);
		$titleValue = $docXml->createTextnode($elementTitle);
		$title->appendChild($titleValue);
		
		$link = $docXml->createElement("link");
		$item->appendChild($link);
		$linkValue = $docXml->createTextnode($elementLink);
		$link->appendChild($linkValue);
		
		$author = $docXml->createElement("author");
		$item->appendChild($author);
		$authorValue = $docXml->createTextnode($elementAuthor);
		$author->appendChild($authorValue);
    }
	$docXml->save("summaryBlogs.xml");
 ?>
            </body>
    </head>
</html>